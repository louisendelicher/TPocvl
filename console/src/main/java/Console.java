import java.util.List;
import java.util.Scanner;
import org.apache.log4j.Logger;
import twitter4j.*;

public class Console {
    private final static Logger logger = Logger.getLogger(Console.class.getName());

    public static void main(String[] args) {
        final Logger logger = Logger.getLogger(Console.class);
        
        int X = 5;
        Scanner sc = new Scanner(System.in);
        
        System.out.print("X personnalisé ? y/n >> ");
        String choice = sc.nextLine();
        
        if(choice.equals("y")) {
            System.out.print("Entrez X : ");
            X = sc.nextInt();
        }
        
        logger.info("X vaut : " + X);
        
        for(int i=0; i<X; i++) {
            logger.info("Valeur fib " + Fibonacci.fib(i));
        }
        
        System.out.println("Tweets #Fibonacci");
        Twitter twitter = new TwitterFactory().getInstance();
        try {
            Query query = new Query("#fibonacci");
            QueryResult result;
        do {
            result = twitter.search(query);
            List<Status> tweets = result.getTweets();
            for (Status tweet : tweets) {
                String t = "@" + tweet.getUser().getScreenName() + " - " + tweet.getText();
                System.out.println(t);
                logger.info(t);
            }
        } while ((query = result.nextQuery()) != null);
            System.exit(0);
        } catch (TwitterException te) {
            logger.error(te.getStackTrace());
        System.out.println("Failed to search tweets : " + te.getMessage());
        System.exit(-1);
        }
    }
}
